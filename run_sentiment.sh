#!/usr/bin/env bash
#python3 create_train.py SENTIMENT_training POS NEG > sentiment_training.txt
#python3 nblearn.py sentiment_training.txt sentiment.nb
python3 create_test.py SENTIMENT_test POS NEG > sentiment_test.txt
python3 nbclassify.py sentiment.nb sentiment_test.txt > sentiment.out
python3 fscore.py sentiment_goldfile sentiment.out POS NEG