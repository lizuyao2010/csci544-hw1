#!/usr/bin/env python3
import sys
if len(sys.argv) < 3:
    print("not enough arguments, example: python3 fscore.py goldfile spam.out SPAM HAM", file=sys.stderr)
    exit(1)
goldfile=sys.argv[1]
myout=sys.argv[2]
tp=0
fp=0
fn=0
tn=0
positive=sys.argv[3]
negative=sys.argv[4]
with open(goldfile) as gf, open(myout) as mo:
    for line in gf:
        r1=line.strip()
        r2=mo.readline().strip()
        if r1 and r2:
            if r1==positive and r2==positive:
                tp+=1
            if r1==positive and r2==negative:
                fn+=1
            if r1==negative and r2==positive:
                fp+=1
            if r1==negative and r2==negative:
                tn+=1
print('tp:',tp,'fp:',fp,'fn:',fn,'tn:',tn)
precision=float(tp)/(tp+fp)
recall=float(tp)/(tp+fn)
f1=2*precision*recall/(precision+recall)
print('f1:',f1)