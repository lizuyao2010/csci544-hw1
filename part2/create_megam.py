import json,sys,collections
# python3 create_megam_test.py spam_test.txt spam_svm_test.txt SPAM HAM

label_dic={'TEST':0}
label_dic[sys.argv[3]]=+1
label_dic[sys.argv[4]]=-1



# read train_file, do word count
file_name=sys.argv[1]
svm_file=sys.argv[2]
with open(file_name,'r',encoding='utf-8') as fp, open(svm_file,'w',encoding='utf-8') as fw:
    for line in fp:
        line=line.strip()
        label,vector=json.loads(line)
        bag=collections.Counter(vector)
        print(label_dic[label],end=' ',file=fw)
        for word in bag:
            print(str(word),str(bag[word]),end=' ',file=fw)
        print(file=fw)

