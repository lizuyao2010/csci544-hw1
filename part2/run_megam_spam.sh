#!/usr/bin/env bash
#python3 create_megam.py spam_training.txt spam_megam_training.txt SPAM HAM
#./megam.opt -maxi 600 binary spam_megam_training.txt > spam.megam.model
python3 create_megam.py spam_test.txt spam_megam_test.txt SPAM HAM 
./megam.opt -predict spam.megam.model binary spam_megam_test.txt > spam_megam_predictions
python3 post_process_megam.py spam_megam_predictions SPAM HAM > spam.megam.out
python3 ../fscore.py ../spam_goldfile spam.megam.out SPAM HAM