#!/usr/bin/env bash
#python3 create_megam.py sentiment_training.txt sentiment_megam_training.txt POS NEG 
#./megam.opt -maxi 300 binary sentiment_megam_training.txt > sentiment.megam.model
python3 create_megam.py sentiment_test.txt sentiment_megam_test.txt POS NEG 
./megam.opt -predict sentiment.megam.model binary sentiment_megam_test.txt > sentiment_megam_predictions
python3 post_process_megam.py sentiment_megam_predictions POS NEG > sentiment.megam.out
python3 ../fscore.py ../sentiment_goldfile sentiment.megam.out POS NEG
