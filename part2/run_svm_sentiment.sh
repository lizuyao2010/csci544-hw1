#!/usr/bin/env bash

python3 create_svm_train.py sentiment_training.txt sentiment_svm_training.txt POS NEG sentiment_svm_words
./svm_learn sentiment_svm_training.txt sentiment.svm.model
python3 create_svm_test.py sentiment_dev.txt sentiment_svm_test.txt POS NEG sentiment_svm_words
./svm_classify sentiment_svm_test.txt sentiment.svm.model sentiment_predictions
python3 post_process_svm.py sentiment_predictions POS NEG > sentiment.svm.out
python3 ../fscore.py ../sentiment_goldfile sentiment.svm.out POS NEG
