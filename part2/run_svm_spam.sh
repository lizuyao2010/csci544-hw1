#!/usr/bin/env bash
#python3 create_svm_train.py spam_training.txt spam_svm_training.txt SPAM HAM spam_svm_words
#./svm_learn spam_svm_training.txt spam.svm.model
python3 create_svm_test.py spam_test.txt spam_svm_test.txt SPAM HAM spam_svm_words
./svm_classify spam_svm_test.txt spam.svm.model spam_predictions
python3 post_process_svm.py spam_predictions SPAM HAM > spam.svm.out
python3 ../fscore.py ../spam_goldfile spam.svm.out SPAM HAM