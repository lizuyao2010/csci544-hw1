#!/usr/bin/env python3
import json,sys
from collections import defaultdict
word_count=defaultdict(lambda : defaultdict(lambda : 1))
label_count=defaultdict(int)

likelihood=defaultdict(lambda : defaultdict(float))
total_count=defaultdict(int)
prior_dist=defaultdict(float)
V=set()
# read train_file, do word count
trainfile_name=sys.argv[1]
with open(trainfile_name,'r',encoding='utf-8') as fp:
    for line in fp:
        line=line.strip()
        label,bag=json.loads(line)
        label_count[label]+=1
        for word in bag:
            word_count[label][word]+=bag[word]
            if word not in V:
                V.add(word)

# generate prior distribution
for label in label_count:
    prior_dist[label]=float(label_count[label])/sum(label_count.values())

# generate likelihood
for label in word_count:
    total_count[label]=sum(word_count[label].values())
    for word in word_count[label]:
        likelihood[label][word]=float(word_count[label][word])/total_count[label]

modelfile_name=sys.argv[2]
with open(modelfile_name,'w') as fw:
    json.dump([likelihood,total_count,prior_dist,len(V)],fw)