#!/usr/bin/env python3
import os,sys,collections,re,json,random


def get_word_vector(label,fp):
    text=fp.read()
    # count word in a word list, return bag of words
    word_vector=re.findall(r'[\w]+',text)
    bigram_vector=[]
    for (i,word) in enumerate(word_vector,0):
        if i+2<=len(word_vector):
            bigram_vector.append('_'.join(word_vector[i:i+2]))
    bag=collections.Counter(bigram_vector)
    print (json.dumps([label,bag]))
    
labels=[]
if len(sys.argv) < 4:
    print("must provide at least 4 argument, example: python3 create_train.py SPAM_training SPAM HAM > TRAININGFILE",file=sys.stderr)
for arg in sys.argv[2:]:
    labels.append(arg)

dir_path=sys.argv[1]
all_files=os.listdir(dir_path)
random.shuffle(all_files)
index=int(len(all_files))
for filename in all_files[:index]:
    filepath=dir_path+'/'+filename
    if os.path.isfile(filepath):
        for label in labels:
            if filename.startswith(label):
                with open(filepath,'r',encoding='utf-8',errors='ignore') as fp:
                    get_word_vector(label,fp)
                    break 