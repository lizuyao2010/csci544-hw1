#1.part 1
## spam dataset: 
        precision:95.0 recall:97.8 F-score:96.9
##sentiment dataset:
        precision:87.1 recall:81.5 F-score:88.5

#2.part 2
##svm:
###spam dataset:
            precision:95.7 recall:79.3 F-score:91.2
###sentiment dataset:
            precision:85.0 recall:86.8 F-score:85.9
##megam:
###spam dataset:
            precision:93.9 recall:80.4 F-score:97.0
###sentiment dataset:
            precision:78.9 recall:79.8 F-score:94.5

#3. only use 10% of training data:
##1.part 1
###spam dataset: 
            precision:94.2 recall:98.6 F-score:96.4
###sentiment dataset:
            precision:86.2 recall:74.9 F-score:80.2

##2.part 2
###svm:
####spam dataset:
                precision:89.8 recall:41.0 F-score:56.3
####sentiment dataset:
                precision:78.4 recall:79.7 F-score:79.0
### megam:
####spam dataset:
                precision:26.6 recall:100 F-score:42.1
#### sentiment dataset:
                precision:75.9 recall:78.8 F-score:77.3
when only 10% of training data is used to train our classifier.
    For naive bayes, it doesn't affect much, only f-score of sentiment drops by 5 percent.
    For SVM, it affects much. For spam, it drops by 30 percent. For sentiment, it drops by 6 percent.
    For Megam, it affects a lot on spam dataset. For spam, it drops by 54 percent. For sentiment, it drops by 3 percent.
    I think it is the size of training set affect the f-score. 10 percent of spam dataset is not big enough to train SVM and Megam. But it seems that it is enough to train naive bayes. Since sentiment data set is 2 times bigger than spam dataset, the test score will not drop a lot.