import json,sys
# python3 create_megam_train.py spam_training.txt part2/spam_megam_training.txt SPAM HAM part2/spam_words

label_dic={}
label_dic[sys.argv[3]]=1
label_dic[sys.argv[4]]=0

word_mapper={}
id_generator=1

# read train_file, do word count
file_name=sys.argv[1]
svm_file=sys.argv[2]
with open(file_name,'r',encoding='utf-8') as fp, open(svm_file,'w',encoding='utf-8') as fw:
    for line in fp:
        line=line.strip()
        label,bag=json.loads(line)
        print(label_dic[label],end=' ',file=fw)
        ls=[]
        for word in bag:
            if word not in word_mapper:
                word_mapper[word]=id_generator
                id_generator+=1
            ls.append((word_mapper[word],bag[word]))
        sorted_ls=sorted(ls,key=lambda pair: pair[0])
        for pair in sorted_ls:
            print('F'+str(pair[0]),str(pair[1]),end=' ',file=fw)
        print(file=fw)

with open(sys.argv[5],'w',encoding='utf-8') as fw:
    json.dump(word_mapper,fw)