#!/usr/bin/env bash
#python3 create_train.py SPAM_training SPAM HAM > spam_training.txt
#python3 nblearn.py spam_training.txt spam.nb
#python3 create_test.py SPAM_dev SPAM HAM > spam_dev.txt
python3 nbclassify.py spam.nb spam_dev.txt > spam.out
python3 fscore.py spam_goldfile spam.out SPAM HAM