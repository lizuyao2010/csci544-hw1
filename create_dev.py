#!/usr/bin/env python3
import os,sys,random,shutil
# example : python3 create_dev.py SENTIMENT_total SENTIMENT_training SENTIMENT_dev 0.1

dir_path=sys.argv[1]


file_list=[]
for filename in os.listdir(dir_path):
    file_list.append(filename)
random.shuffle(file_list)

percent=float(sys.argv[4])
n=len(file_list)
for (i,filename) in enumerate(file_list):
    src=dir_path+'/'+filename
    # dev set
    if i < n*percent:
        tar=sys.argv[3]+'/'+filename
        shutil.copyfile(src,tar)
    # trainning set
    else:
        tar=sys.argv[2]+'/'+filename
        shutil.copyfile(src,tar)

