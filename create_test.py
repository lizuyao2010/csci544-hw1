#!/usr/bin/env python3
import os,sys,collections,re,json


def get_word_vector(label,fp):
    text=fp.read()
    # word list with duplicates
    word_vector=re.findall(r'[\w]+',text)
    bigram_vector=[]
    for (i,word) in enumerate(word_vector,0):
        if i+2<=len(word_vector):
            bigram_vector.append('_'.join(word_vector[i:i+2]))
    #bag=collections.Counter(bigram_vector)
    print (json.dumps([label,bigram_vector]))

labels=['TEST']
if len(sys.argv) < 4:
    print("must provide at least 4 argument, example: python3 create_test.py SPAM_dev SPAM HAM > TRAININGFILE",file=sys.stderr)
for arg in sys.argv[2:]:
    labels.append(arg)

dir_path=sys.argv[1]
all_files=os.listdir(dir_path)
all_files=sorted(all_files)
for filename in all_files:
    if dir_path.endswith('/'):
        filepath=dir_path+filename
    else:
        filepath=dir_path+'/'+filename
    if os.path.isfile(filepath):
        for label in labels:
            if filename.startswith(label):
                with open(filepath,'r',encoding='utf-8',errors='ignore') as fp:
                    get_word_vector(label,fp)
                    break 
       